/**
 * @file visualizer.js
 *
 * Defines the behavior of the dependency visualizer module.
 */
(function (Drupal, drupalSettings) {

  'use strict';

  /**
   * Initializes the vis visualizer
   */
  Drupal.behaviors.visualizer = {
    nodes: null,
    edges: null,
    network: null,
    attach: function () {
      this.draw();
    },
    destroy: function () {
      if (this.network !== null) {
        this.network.destroy();
        this.network = null;
      }
    },
    draw: function () {
      this.destroy();
      this.nodes = [];
      this.edges = [];


      let edgeList = drupalSettings.dependency_visualizer.edges;
      let nodeList = drupalSettings.dependency_visualizer.nodes;
      let errorList = drupalSettings.dependency_visualizer.errors;
      let nodeCount = drupalSettings.dependency_visualizer.nodes.length;

      for (let i = 0; i < nodeCount; i++) {
        let node = String(nodeList[i]);
        let dependencies = edgeList[node];
        let hasError = errorList.hasOwnProperty(node);

        this.nodes.push({
          id: node,
          label: node,
          color: hasError ? '#FFCCCB' : '#D2E5FF',
          title: hasError ? String(errorList[node]) : undefined,
        });

        if (!dependencies.length) {
          continue;
        }

        for (let j = 0; j < dependencies.length; j++) {
          let to = dependencies[j];
          this.edges.push({
            from: node,
            to: to,
            arrows: {to: true}
          });
        }
      }

      let container = document.getElementById('network');
      let data = {
        nodes: this.nodes,
        edges: this.edges
      };
      let options = {
        height: '1000px',
        width: '100%'
      };
      this.network = new vis.Network(container, data, options);
    }

  };

})(Drupal, drupalSettings);






