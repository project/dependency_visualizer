<?php

namespace Drupal\dependency_visualizer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Yaml\Yaml;

/**
 * Class VisualizerController.
 *
 * Visualize module dependency graph.
 */
class VisualizerController extends ControllerBase {

  /**
   * Visualize.
   *
   * @return array
   *   Returns the visualizer.
   */
  public function visualize() {
    $nodes = [];
    $edges = [];
    $errors = [];
    $moduleHandler = \Drupal::moduleHandler();

    foreach ($moduleHandler->getModuleList() as $extension) {
      $moduleName = $extension->getName();
      $dependencies = $this->parseDependencies($moduleName, $extension->getType());

      foreach ($dependencies as $dependency) {
        if (!$moduleHandler->moduleExists($dependency)) {
          $nodes[] = $dependency;
          $edges[$dependency] = [];
          $errors[$dependency] = 'Module is not installed';
        }
      }

      $nodes[] = $moduleName;
      $edges[$moduleName] = $dependencies;
    }

    return [
      'network' => [
        '#type' => 'container',
        '#attributes' => ['id' => 'network'],
      ],
      '#attached' => [
        'library' => 'dependency_visualizer/visualizer',
        'drupalSettings' => [
          'dependency_visualizer' => [
            'nodes' => $nodes,
            'edges' => $edges,
            'errors' => $errors,
          ],
        ],
      ],
    ];
  }

  /**
   * Parse dependencies of given module or profile.
   *
   * @param string $module
   *   Module or profile name.
   * @param string $type
   *   Extension type: module or profile.
   *
   * @return array
   *   List of dependencies.
   */
  private function parseDependencies(string $module, string $type): array {
    $filename = drupal_get_path($type, $module) . "/$module.info.yml";
    $info = Yaml::parseFile($filename);
    if ($type === 'profile') {
      return isset($info['install']) ? $this->cleanModuleName($info['install']) : [];
    }
    return isset($info['dependencies']) ? $this->cleanModuleName($info['dependencies']) : [];
  }

  /**
   * Remove namespace from module names.
   *
   * @param array $modules
   *   Module names.
   *
   * @return array
   *   Clean module names.
   */
  private function cleanModuleName(array $modules): array {
    $cleaned = [];
    foreach ($modules as $module) {
      $clean = explode(':', $module)[1] ?? $module;
      $cleaned[] = explode(' ', $clean)[0] ?? $module;
    }
    return $cleaned;
  }

}
